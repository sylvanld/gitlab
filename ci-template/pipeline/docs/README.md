# Documentation pipelines

## Mkdocs

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: 'v1.20'
    file: '/ci-template/pipeline/docs/mkdocs.yaml'
```
