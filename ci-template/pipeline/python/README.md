# Python pipeline templates

## Quality analysis

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}
    file: '/ci-template/pipeline/python/quality-analysis.yaml'

stages:
  - quality analysis
  - quality report

variables:
  PYTHON_IMAGE_TAG: python:3.8-alpine
  PYTHON_LINT_FOLDERS: octoauth
```

Create a requirement file with at least:
```
bandit==<bandit_version>
pytest==<pytest_version>
pytest-cov==<pytest_cov_version>
pylint==<pylint_version>
pylint-exit==<pylint_exit_version>
```

## Release

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/pipeline/python/release.yaml'

stages:
  - release

variables:
  PYTHON_IMAGE_TAG: python:3.8-alpine
  PYTHON_TWINE_VERSION: 4.0.1
```
