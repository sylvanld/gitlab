# Soanr Templates

## Sonar scanner

Run sonar-scanner and export analysis to remote sonarqube instance.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/sonar/scanner.yaml'

sonar-analysis:
  extends: .sonar_scanner
  variables:
    SONAR_PROJECT_KEY: <key>
    SONAR_SOURCE: myapp/
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|SONAR_PROJECT_KEY|Key used to identity project in sonarqube|yes|-|
|SONAR_SOURCE|Path to the source code of your application|yes|-|
|SONAR_HOST_URL|URL of sonarqube instance. (Prefer to set it at group level)|yes|-|
|SONAR_TOKEN|Token used to authenticate through sonarqube|yes|-|
