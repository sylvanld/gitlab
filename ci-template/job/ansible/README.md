# Ansible Templates

## Run Ansible Playbook

Run ansible playbook using ansible runner image.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/ansible/playbook.yaml'

configuration-job:
  extends: .ansible_playbook
  variables:
    ANSIBLE_INVENTORY: inventory
    ANSIBLE_PLAYBOOK: playbook.yaml
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|ANSIBLE_INVENTORY|Path to ansible inventory (file or folder describing target machines)|no|inventory|
|ANSIBLE_PLAYBOOK|Path to the playbook that will be executed|no|playbook.yaml|
