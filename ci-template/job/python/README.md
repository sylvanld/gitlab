# Python Templates

## Python base job

Base job that handles caching and packages installation for python packages. It is used by all python linting/test jobs.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/job.yaml'

python-job:
  extends: .python_job
  variables:
    PYTHON_IMAGE_TAG: '3.8-alpine'
    PYTHON_REQUIREMENTS: '-r requirements.txt'
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|PYTHON_REQUIREMENTS|Space separated list of python requirements. If you prefer to use a requirement file, you can specify `-r my_requirements` as you would do with `pip install ...`|no|empty (no requirements installed)|
|PYTHON_IMAGE_TAG|Version of python image to use. Must refer to a valid tag for [docker python image](https://hub.docker.com/_/python?tab=tags).|no|3.8-slim|

---

## Python Linter: Pylint

Check quality of your code using pylint. Output a coverage score that can be used to [generate a badge](#get-pylint-badge).

Inherits [Python base job](#python-base-job) behaviour.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/job.yaml'
  
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/pylint.yaml'

python-pylint:
  extends: .python_pylint
  variables:
    PYTHON_LINT_FOLDERS: "my-project/"
    PYTHON_PYLINT_VERSION: "2.12.2"
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|PYTHON_LINT_FOLDERS|Specify source folder of your application that must be linted.|no|. (current directory)|
|PYTHON_PYLINT_VERSION|You can use this variable to customize version of pylint that must be used|no|2.12.2|
|PYTHON_PYLINT_EXIT_VERSION|You can use this variable to customize version of pylint-exit (CLI to normalize pylint exit code) that must be used|no|1.2.0|

### Get pylint badge

Use the following URL to get your badge:

```
https://<gitlab-host>/<path-to-project>/badges/<branch>/coverage.svg?job=<job-name>&key_text=pylint+score&key_width=110
```

---

## Python Linter: Black

Job used to verify that source code respect coding convention stated by [black formatting provider](https://black.readthedocs.io/en/stable/).

Inherits [Python base job](#python-base-job) behaviour.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/job.yaml'
  
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/black.yaml'

python-black:
  extends: .python_black
  variables:
    PYTHON_LINT_FOLDERS: "my-project/"
    PYTHON_BLACK_VERSION: "22.1.0"
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|PYTHON_LINT_FOLDERS|Directory(ies) where isort will verify import order|no|. (current directory)|
|PYTHON_BLACK_VERSION|Version of black used for the check|no|22.1.0|

---

## Python Linter: Isort

Ensure import order respect conventions defined in [PEP8](https://www.python.org/dev/peps/pep-0008/#imports)

Inherits [Python base job](#python-base-job) behaviour.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/isort.yaml'

python-isort:
  extends: .python_isort
  variables:
    PYTHON_LINT_FOLDERS: "my-app/"
    PYTHON_ISORT_VERSION: "5.10.1"
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|PYTHON_LINT_FOLDERS|Directory(ies) where isort will verify import order|no|. (current directory)|
|PYTHON_ISORT_VERSION|Version of isort used for the check|no|5.10.1|

---

## Python Publish Release (Twine)

Build python package from current folder then use twine to upload it to PyPI.

Inherits [Python base job](#python-base-job) behaviour.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/job.yaml'
  
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/release/publish.yaml'

python-publish-release:
  extends: .python_publish_release
  variables:
    PYTHON_IMAGE_TAG: python:3.8-slim
    PYTHON_TWINE_VERSION: 4.0.1
  rules:
    - if: $CI_COMMIT_TAG != null
      variables:
        PYTHON_PACKAGE_VERSION: $CI_COMMIT_TAG
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|PYTHON_REGISTRY_URL|URL of the registry where the package will be uploaded|yes|-|
|PYTHON_REGISTRY_USER|Username used to authenticate through python registry|yes|-|
|PYTHON_REGISTRY_TOKEN|Token or password used to authenticate through python registry|yes|-|
|PYTHON_PACKAGE_VERSION|Version of the package to be built and deployed|yes|-|
|PYTHON_TWINE_VERSION|Version of twine used for this job ([have a look at pypi for existing versions](https://pypi.org/project/twine/#history))|no|4.0.1|

---

## Python Unpublish Release

Remove a published release from python registry.

### Usage

```yaml
include:  
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/python/release/unpublish.yaml'

python-unpublish-release:
  extends: .python_unpublish_release
  rules:
    - if: $CI_COMMIT_TAG != null
      when: manual
      variables:
        PYTHON_PACKAGE_NAME: mypackage
        PYTHON_PACKAGE_VERSION: $CI_COMMIT_TAG
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|PYTHON_REGISTRY_URL|URL of the registry where the package will be uploaded|yes|-|
|PYTHON_REGISTRY_USER|Username used to authenticate through python registry|yes|-|
|PYTHON_REGISTRY_TOKEN|Token or password used to authenticate through python registry|yes|-|
|PYTHON_PACKAGE_NAME|Name of the package whose release will be removed|yes|-|
|PYTHON_PACKAGE_VERSION|Version of the package to be unpublished from registry|yes|-|

---
