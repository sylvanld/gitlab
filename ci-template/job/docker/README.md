# Docker Templates

## Release Docker Image

Build and release a docker image to the given registry.

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/docker/release.yaml'

docker-release: 
  extends: .docker_release
  variables:
    DOCKER_BUILD_PLATFORMS: "linux/amd64,linux/arm64"
    DOCKER_IMAGE_NAME: "sylvanld/demo-image"
    # uncomment to provide docker registry information directly at job level
    # DOCKER_USERNAME: "${ARTIFACTORY_USERNAME}"
    # DOCKER_PASSWORD: "${ARTIFACTORY_PASSWORD}"
    # DOCKER_REGISTRY: "${ARTIFACTORY_REGISTRY}"
  rules:
    - if: $CI_COMMIT_TAG != null
      when: always
      variables:
        DOCKER_BUILD_ARGS: "NGINX_TAG=$CI_COMMIT_TAG MAINTAINER=me"
        DOCKER_IMAGE_TAG: "$CI_COMMIT_TAG"
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|DOCKER_BUILD_ARGS|Space separated args passed to docker build command (in the format "variable=value")|no|-|
|DOCKER_BUILD_PLATFORMS|Coma separated set of platforms (architecture) for which image should be built|no|linux/amd64|
|DOCKER_IMAGE_NAME|Name of the image to be built|yes|-|
|DOCKER_IMAGE_TAG|Tag of the image to be built|yes|-|
|DOCKER_REGISTRY|Docker registry URL|no|docker.io|
|DOCKER_USERNAME|Docker registry username|yes|-|
|DOCKER_PASSWORD|Docker registry password|yes|-|

> Note: the recommended way to specify registry information is to define it as [CI variables at group level](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-group) to be available in all child projects.
