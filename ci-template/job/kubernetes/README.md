# Kubernetes Templates

## Kubectl Apply

### Usage

```yaml
include:
  - project: 'sylvanld/gitlab'
    ref: '{{reference}}'
    file: '/ci-template/job/kubernetes/apply.yaml'

kubectl-apply:
  extends: .kubectl_apply
  variables:
    KUBERNETES_MANIFEST: kubernetes/my-service.yaml
    KUBERNETES_NAMESPACE: my-namespace
```

### Variables

|Variable name|Description|Is required|Default value|
|-|-|-|-|
|KUBERNETES_MANIFEST|Path to kubernetes manifest that describes wanted state.|yes|-|
|KUBERNETES_CONFIG_PATH|Path to K8S config file used to authenticate through remote cluster. This can be set by creating a gitlab [file variable](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types)|yes|-|
|KUBERNETES_NAMESPACE|Namespace in which manifests will be applied|no|default|
